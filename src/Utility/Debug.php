<?php
/**
 * Created by PhpStorm.
 * User: Rashed Alam
 * Date: 4/12/2018
 * Time: 10:14 PM
 */

namespace Pondit\Utility;


class Debug
{
    static function dd($variable){
        echo "<pre>";
        echo $variable;
        echo "</pre>";
        die();
    }
}